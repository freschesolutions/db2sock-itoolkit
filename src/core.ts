/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

import { HttpProvider } from './providers/HttpProvider'
import { IDataProvider } from './interfaces/IDataProvider'

import { Cmd } from './cmd'
import { Pgm } from './pgm'
import { Qsh } from './qsh'

export class Core {
    private m_Provider: HttpProvider;
    private m_Callers: IDataProvider[];

    constructor(remoteUrl: string)
    {
        this.m_Provider = new HttpProvider(remoteUrl);
        this.m_Callers = [];
    }

    get provider(): HttpProvider {
        return this.m_Provider;
    }

    add(caller: IDataProvider): void {
        this.m_Callers.push(caller);
    }

    run(done: (result: object) => void): void {
        // Build a list of calls to send to the server
        let requestData = this.getRequestData();

        // Check if we have a connection to make as part of our call list
        // TODO: Do this
        
        // Submit the request to the server
        this.m_Provider.send(requestData, done);
    }

    getRequestData(): object
    {
        let script: {'script': Array<object>} = {
            'script': []
        };
        
        this.m_Callers.forEach((value) => {
            script['script'].push(value.getRequestData());
        });

        return script;
    }

    command(command: string): Cmd {
        return new Cmd(command);
    }

    program(program: string, library: string, func?: string): Pgm {
        return new Pgm(program, library, func);
    }

    shell(command: string): Qsh {
        return new Qsh(command);
    }
}