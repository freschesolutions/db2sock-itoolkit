/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

import { IDataProvider } from './interfaces/IDataProvider'

export class Qsh implements IDataProvider {
    private m_Command: string;

    constructor(command: string) {
        this.m_Command = command;
    }

    getRequestData(): object {
        return { 'cmd': { 'qsh': this.m_Command } };
    }
}