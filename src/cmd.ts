/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

import { IDataProvider } from './interfaces/IDataProvider'
import * as request from 'request';

export class Cmd implements IDataProvider {
    private m_Command: string;

    constructor(command: string)
    {
        this.m_Command = command;
    }

    getRequestData(): object {
        // If the command contains return parameters, call rexx
        if (this.m_Command.indexOf('?') > 0) {
            return { 'cmd': { 'rexx': this.m_Command } };
        } else {
            return { 'cmd': { 'exec': this.m_Command } };
        }
    }
}