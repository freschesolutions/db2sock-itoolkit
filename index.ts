/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

import { Core } from "./src/core";

export { Core };
export default Core;
